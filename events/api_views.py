from common.json import ModelEncoder
from .models import Conference, Location, State
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "state",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "id",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})

    # this if-block handles the GET request
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    # this else-block handles the POST reques
    else:
        content = json.loads(request.body)

        # Get the Location object an dput it in the content dictionary
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        # if the location does not exist, return error message response
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, pk):
    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    # conference_detail = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": conference_detail.name,
    #         "starts": conference_detail.starts,
    #         "ends": conference_detail.ends,
    #         "description": conference_detail.description,
    #         "created": conference_detail.created,
    #         "updated": conference_detail.updated,
    #         "max_presentations": conference_detail.max_presentations,
    #         "max_attendees": conference_detail.max_attendees,
    #         "location": {
    #             "name": conference_detail.location.name,
    #             "href": conference_detail.location.get_api_url(),
    #         },
    #     }
    # )

    # conference = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     conference,
    #     encoder=ConferenceDetailEncoder,
    #     safe=False,
    # )

    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference"},
                status=400,
            )

        conference = Conference.objects.update(**content)
        conference = Conference.objects.get(id=pk)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"locations": response})

    # locations = [
    #     {
    #         "name": location.name,
    #         "href": location.get_api_url(),
    #     }
    #     for location in Location.objects.all()
    # ]
    # return JsonResponse({"locations": locations})

    # if block: if request method is GET, return
    # all instances of location objections
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    # this else block handles the POST
    else:
        # decodes content of POST (JSON-formatted string
        # stored in request.body) to dictionary
        content = json.loads(request.body)

        try:
            # Get the State object and put it in the content dict
            # (b/c in Location model, state is Foreign Key, not a string):
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # create a new Location
        location = Location.objects.create(**content)
        # return newly-created Location
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # location_detail = Location.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": location_detail.name,
    #         "city": location_detail.city,
    #         "room_count": location_detail.room_count,
    #         "created": location_detail.created,
    #         "updated": location_detail.updated,
    #         "state": location_detail.state.abbreviation,
    #     }
    # )

    # location_detail = Location.objects.get(id=pk)
    # return JsonResponse(
    #     {"location_detail": location_detail},
    #     encoder=LocationListEncoder,
    # )

    # this if block only applicable to GET
    if request.method == "GET":
        location_detail = Location.objects.get(id=pk)
        return JsonResponse(
            location_detail,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    # this elif block tests if the HTTP method is DELETE
    elif request.method == "DELETE":
        # the delete() looks for the location's id=pk to delete
        # the methode deletes the object and returns:
        # (# of obj deleted, {'object type': #deletions })
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    # this else block is for PUT
    else:
        # copied from create (PUT) in api_list_locations
        # convert JSON-formatted string into dictionary
        content = json.loads(request.body)

        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code; updates
        Location.objects.filter(id=pk).update(**content)

        # copied from get detail
        location_detail = Location.objects.get(id=pk)
        return JsonResponse(
            location_detail,
            encoder=LocationDetailEncoder,
            safe=False,
        )
